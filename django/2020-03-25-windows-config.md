---
title:  Django Config [Windows]
subtitle: Configuring the development environment
date: 2020-03-25
tags: ["django","python", "web development", "backend framework", "backend","windows"]
---

<a id="org11e18f2"></a>


This Guide will helps you to configure simple development env in your `Windows Operating System`.
You can see that there are only small changes in the configuration process while comparing to `Linux and Mac Operating Systems`.

- In this part we will first Install `Python`,
- Then configure the `PIP` using `python` and `get-pip` scripts
- Then we will configure and install our environment using `pip` and `virtualenv`

- Finally we Install Django using PIP


### 1. Download Install Python

- Download the stable version of python 3.x from [here](https://www.python.org/downloads/windows/) and Install it.

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-download.png" width="600">

.

- Excecute the command by opening command prompt or directly install using downloaded installer

  `python-3.6.0.exe /quiet InstallAllUsers=1 PrependPath=1 Include_test=0`

  - Open the CMD

    (wait few minutes and close the cmd it will automatically install, then reopen).

    <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-cmd.png" width="600">   

    .

  - Install the python
  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-pyinstall.png" width="600">

    >`Warning !`
    wait few minutes(~2 - ~5 minutes) and close the cmd it will automatically install,
    You will not get any sucess notification as this is a silent installation(using cmd),after few minutes reopen CMD.


- Check the Installation

  `python --version`

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-py.png" width="600">

### 2. Configure PIP

- Use the commands below or Save this >> [pip-for-windows](https://bootstrap.pypa.io/get-pip.py) << web page as get-pip.py file

  `curl https://bootstrap.pypa.io/get-pip.py > get-pip.py`

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-pipinstall.png" width="600">

  .
- Run the Downloaded Scripts using python i.e

  `python get-pip.py`

    <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-pip-config.png" width="600">

    >`Note`
    You may need to reopen the cmd for Checking the installation.

- Check the pip installation

  `pip --version`

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-pip.png" width="600">

### 3. Configure Virtualenv

- Install Virtualenv

  `pip install virtualenv`

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-venv.png">

  >`Note`
  You may need to reopen the cmd for Checking the installation.

- Check the Installation Status

  `virtualenv --version`

  <img src="https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-venvs.png">

### 4. [continued here](django/2020-03-18-getting-started#)

[wd]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/win-download.png  "download"
