---
title:  Writing First Django App [ Django Project - 1 ]
subtitle: Learn to Say Hello in Django
date: 2020-03-27
tags: ["django","python", "web development", "backend framework", "backend","django app", "django project"]
---

<a id="org11e18f2"></a>


We are going to write our first Web App in Django. Before that, we can try to get a simple idea on Django, i.e how Django works when we request a website, which is built using Django. Also, what are the basic codes required for viewing a web page?

- We will understand how a basic Django project process the requests from clients (users).
- We will familiarize the default apps available within Django, such as admin and user modules.
- We will write a small app which will deliver some messages from you to visitors
- We will dive deep into Django very slowly, so if you are in a hurry
I recommend you to follow. [simple better than complex](https://simpleisbetterthancomplex.com/series/beginners-guide/1.11/), [django-girls](https://tutorial.djangogirls.org/en/)



### 1. Understand the Django Things

- how a basic project process's works

  ![][basic-workflow]

  image credits
  [ref](https://www.slideshare.net/ducminhkhoi/pythondjango-training)

- Create Your First app ( just for understanding the structure)

  `django-admin startapp firstapp`

  > Your Expected Working Directory Structure after this command
    ``

    ```Project Directory
  ├── mywork
  │   ├── myproject
  │   │   ├── myproject
  |   |   |   ├── _ _pycache_ _  
  |   |   |   ├── _ _init_ _.py
  |   |   |   ├── asgi.py
  |   |   |   ├── settings.py
  |   |   |   ├── urls.py
  |   |   |   └── wsgi.py
  |   |   ├── firstapp
  |   |   |   ├── migrations ( keeps your db changes history)
  |   |   |   ├── _ _init_ _.py
  |   |   |   ├── admin.py ( helps to add your app models to admin dashboard ! we need it )
  |   |   |   ├── apps.py
  |   |   |   ├── models.py ( define db Structure according to our requirements ! we need it)
  |   |   |   ├── tests.py ( define test cases )
  |   |   |   └── views.py ( process the requests recieved from <urls.py> , interact with models ! we need it)
  |   |   ├── db.sqlite3 ( default db)
  |   |   └── manage.py
  │   ├── myenv
  │   └── requirements.txt
  └──
  ```


  -  Familiarize default application available in Django (Before Working on Newly Created One)

    - Open the settings.py
    ```Project Directory
  ├── mywork
  │   ├── myproject
  │   │   ├── myproject
  │   ├──       ├── settings.py
  └──
  ```
    - Check the codes at `INSTALLED_APPS`

    ```
    # Application definition

    INSTALLED_APPS = [
      'django.contrib.admin', <The admin site, by default you can access it from 127.0.0.1:8000/admin >
      'django.contrib.auth', <An authentication system. >
      'django.contrib.contenttypes', < A framework for content types.>
      'django.contrib.sessions',< A session framework.>
      'django.contrib.messages', < A messaging framework.>
      'django.contrib.staticfiles', <  A framework for managing static files.>
      ]

    ```
    - Access Django default Administration : - (on your Terminal)

      `python3 manage.py createsuperuser`

      >input a simple username and password

      ![][access-admin]

      >`Note`
      Password will not be visible while inputing in the terminal

      `python3 manage.py runserver`

       open the URL : http://127.0.0.1:8000/admin on your browser

      `CTRL+Z` < will helps to exit from the server>

       ![][access-admin-2]

      > You will get following options under the Django Administration by default.

      ![][access-admin-3]
      ![][access-admin-4]

        - Authentication modules (manage Authentication,User session)
          - Groups (Helps to create user groups)
          - User (adding and editing users and persmission for users)

    - Understand Some more deep about django apps
      - Open `urls.py`
    ```Project Directory
    ├── mywork
    │   ├── myproject
    │   │   ├── myproject
    │   ├──       ├── urls.py
    └──
    ```

     - Check the codes


    ```
        from django.contrib import admin
        from django.urls import path

        urlpatterns = [
            path('admin/', admin.site.urls), < admin site>
        ]

    ```     

    > For a simple understanding , Above line of codes helps to identify the url end with /admin and serv the Administration option at that urls,then url will be resolved at `admin.site.urls` then which resolved at admin apps `views.py`, then view will be delivered with the help of `models.py` and the `db`, for more just understand it from below workflow.


   ![][django-basic-workflow]

    > `Note`
    The above workflow only be used for basic understanding there are lots of other background process, We are skipping these for just get an idea what are we writing in our project codebase

    - Observ the current database and tables.

      > the default db is always sqlite3 db.
      type following commands to access the default db

        `python manage.py dbshell`

      > a sqlite shell will be opend and enter the following command to list current tables present in sqlite

          `.tables`

        ![][sqlite-db]

      > These tables were created by default by django , when we make the first migration , these tables will be automatically Created.We can remove some of tables ,if we don't want, by uncommenting the apps in
      `INSTALLED_APPS` section before making migrations.

      > to get the table structure or schema enter the following command
      this will print the fields and field types related data.

        `.schema <tablename>`

        ![][sqlite-schema]
      >to get the values from one of the table as line by line.

        `.mode line`

        `select * from <tablename> ;`

        `.exit` <you can exit the shell>

      ![][sqlite-table-data]
      > see the output of `auth_user` table , our `superuser` entry is listed there.

  - **Final Conculsion on understanding Django**

    - A django project run with many apps, or at least with the core  app.
    - all apps should be listed at `settings.py`
    - core settings can be found at `settings.py`
    - main url definition can be found in `urls.py`
    - `views.py` will resolve each urls.
    - `models.py` hepls to create  tables and db queries
    - `db.sqlite3` is the default db for Django
    - `urls.py > views.py > models.py ` this will be the process flow   when a user  fire the requests to django web app.
    - so our coding phase will be like `models.py > views.py > urls.py` along with `settings.py`
    - `admin.py` will helps to add our app to django administration board, there we can edit, create , update one object of a model ( take the user object from above the above examples )

    > `Warning`
    as a beginner these much things are needed for writing a first app which will operate the functions like create, read,update,delete (CRUD). there are `serializers`( api related ops),`templates` (manage html templates) ,`forms` ,(manage user inputs) and `tests` pending to cover. We will try to cover these by upcoming tutorials.

### 2. Fire Up Your First Django App

- always start by wishing hello,
  - write the following lines to `views.py` in `firstapp` (we created while understanding django, if `firstapp` is not created , goto above section findout how to create a django app ,NB: Name of the app can anything).

      ```Views.py  
      from django.http import HttpResponse

      def hello(request):
        return HttpResponse("<b>Hello, Here is my first App</b>")
      ```

      ```Project Directory
    ├── mywork
    │   ├── myproject
    │   │   ├── firstapp
    │   ├──       ├── views.py
    └──
      ```
  - write the following to urls.py

    ```urls.py
    from firstapp import views as firstapp_views

    urlpatterns = [
      .....,
      path('', firstapp_views.hello, name="home page"),
      ....,
      ]
    ```
    .

  ```Project Directory
  ├── mywork
  │   ├── myproject
  │   │   ├── myproject
  │   ├──       ├── urls.py
  └──
  ```
  - Now save these changes and run the server `python manage.py runserver`
    open your browser with the url 127.0.0.1:8000

    ![][hello-firstapp]

    >`Notes on views.py :-`
    we  called `HttpResponse` method from `django.http` , so that we can simply return response which can be formatted using `html tags` (here we used <b> tag for making the response bold. )

    > `Notes on urls.py :-` we wrote the response now we need to show the message when we enter our url, we decided to show that at root url i.e `127.0.0.1:8000`. first we imported our `view` from `firstapp`
    then inside the `urlpatterns` we defined another path like `/admin`, i.e `''` called the view with `name` i.e `home page`,

    >  `Practice :- ` try to serv different messages at different url end points. use the following doc to Learn more about `HttpResponse` method.
    [django.http.HttpResponse](https://docs.djangoproject.com/en/3.0/ref/request-response/#httpresponse-objects)

    > `Final Notes` :-
    we just coverd a small portion of django, still we have to write models in `models.py` also we did not list our app in `settings.py`.
    so these things can be discussed , upcoming parts.

### 2. Make model to store your messages [continued]()



[basic-workflow]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/process-django.jpg  "Process requests from users"
[default-apps]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/default-apps.png "PreDefined apps available in Django"
[access-admin]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/access-admin.png "Create Super User"
[access-admin-2]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/access-admin-2.png "Access Admin Panel"
[access-admin-3]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/access-admin-3.png "Access Admin Panel Home"
[access-admin-4]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/access-admin-4.png "Access Admin Panel Users"
[django-basic-workflow]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/django-basic-workflow.jpg "Django basic workflow"
[sqlite-db]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/sqlite-db.png "default database"
[sqlite-tables]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/sqlite-tables.png "default tables"
[sqlite-schema]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/sqlite-schema.png "default table schema"
[sqlite-table-data]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/sqlite-table-data.png "default tables"
[hello-firstapp]: https://gitlab.com/studevsoc/sds-codealong/-/raw/master/django/images/hello-firstapp.png "hello firstapp"
