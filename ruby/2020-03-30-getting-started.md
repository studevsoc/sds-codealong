---
title: Getting Started with Ruby
subtitle: Installing ruby
date: 2020-03-27
tags: ["ruby",]
---

# Contents

1.  [Introduction](#rubyCTHul1vtBJ)
2.  [Installing Ruby](#rubyyxwtcWxPzu)
3.  [Referrals](#ruby6ZDuTClrSW)


<a id="rubyCTHul1vtBJ"></a>

# Introduction

  [Ruby](https://www.ruby-lang.org/en/) is an interpreted, high-level, general-purpose programming language. It was designed and developed in the mid-1990s by [Yukihiro Matsumoto](https://en.wikipedia.org/wiki/Yukihiro_Matsumoto) in Japan. Ruby is a language of careful balance.  Yukihiro Matsumoto blended parts of his favorite languages (Perl, Smalltalk, Eiffel, Ada, and Lisp) to form a new language that balanced functional programming with imperative programming. Ruby is ranked among the top 10 on most of the indices that measure the growth and popularity of programming languages worldwide.

## Seeeing Everything as an object

  Yukihiro Matsumoto once said "I wanted a scripting language that was more powerful than Perl, and more object-oriented than Python2.”. In ruby everything is an object ans thus every bit of code can be given their own properties and actions. In object oriented programming this property is called instance variables and actions are called methods. This pure object oriented approach can be demonstrated by this code below.

  ```
  5.times {puts "Ruby is awesome!" }
  ```
  Here we can see a methods `times` is called on `5`. Want to read [more](https://www.ruby-lang.org/en/about/) ?

<a id="rubyyxwtcWxPzu"></a>

# Installing Ruby
  
## Windows platform
  For installing ruby on windows we are going to use the ruby installer. The ruby installer is the most widely used installer for windows, please download it from [ruby-installer](https://rubyinstaller.org/). For more details on ruby Installer checkout their wiki [FAQ](https://github.com/oneclick/rubyinstaller2/wiki/faq).

## Linux Distros
  There are a various methods to install ruby.
  
  1. [Packet management systems](https://www.ruby-lang.org/en/documentation/installation/#package-management-systems)
  2. [Installers](https://www.ruby-lang.org/en/documentation/installation/#installers)
  3. [Managers](https://www.ruby-lang.org/en/documentation/installation/#managers)
  4. [Building from the source](https://www.ruby-lang.org/en/documentation/installation/#building-from-source)
  
   I personally prefer using [managers](https://www.ruby-lang.org/en/documentation/installation/#managers). Please find a method which is the most suitable for you and lets get started.


<a id="ruby6ZDuTClrSW"></a>

# Referrals:

* [Official Ruby Documentation](https://www.ruby-lang.org/en/about/)
* [Ruby Installer](https://rubyinstaller.org/)
